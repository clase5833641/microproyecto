using System;
using System.Collections.Generic;

[System.Serializable]
public class DialogueNode
{
    public string dialogueText;
    public List<DialogueResponse> responses;

    internal bool IsLastNode()
    {
      
        if (responses == null || responses.Count == 0)
        {
            return true;
        }
        else
        {
            foreach (DialogueResponse response in responses)
            {
                if (response.nextNode != null)
                {
                    return false;
                }
            }
            return true;
        }
    }

}