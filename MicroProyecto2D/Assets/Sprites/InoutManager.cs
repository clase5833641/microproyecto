using UnityEngine;

public class InputManager : MonoBehaviour
{
    private static InputManager instance;

    // Flag to determine if inputs should be locked
    private bool inputsLocked = false;

    public static InputManager Instance
    {
        get
        {
            // If the instance is null, find it in the scene
            if (instance == null)
            {
                instance = FindObjectOfType<InputManager>();

                // If the instance is still null, create a new GameObject with InputManager attached
                if (instance == null)
                {
                    GameObject obj = new GameObject("InputManager");
                    instance = obj.AddComponent<InputManager>();
                }
            }
            return instance;
        }
    }

    // Lock inputs
    public void LockInputs()
    {
        inputsLocked = true;
    }

    // Unlock inputs
    public void UnlockInputs()
    {
        inputsLocked = false;
    }

    // Check if inputs are locked
    public bool InputsAreLocked()
    {
        return inputsLocked;
    }
}
