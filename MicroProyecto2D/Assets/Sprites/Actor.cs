using System.Collections.Generic;
using TMPro;
using UnityEngine;



public class Actor : MonoBehaviour
{
    public string Name;
    public List<Dialogue> Dialogues; // List of dialogues associated with this actor
    public TextMeshProUGUI interactionText;
    private bool canInteract = false;
    private BossController bossController;
    public GameObject boss1;

    private void Start()
    {
        bossController = GetComponent<BossController>();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !this.CompareTag("boss"))
        {
            interactionText.gameObject.SetActive(true);
            interactionText.text = "Press E to interact";
            canInteract = true;
        }
        if (other.CompareTag("Player") && this.CompareTag("boss"))
        {
            if (bossController.HP == 0)
            {
                interactionText.gameObject.SetActive(true);
                interactionText.text = "Press E to interact";
                canInteract = true;
            }

        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !this.CompareTag("boss"))
        {
            interactionText.gameObject.SetActive(false);
            canInteract = false;
        }
        if (other.CompareTag("Player") && this.CompareTag("boss"))
        {
            interactionText.gameObject.SetActive(false);
            canInteract = false;
        }
    }
    private void Update()
    {
        if (canInteract && Input.GetKeyDown(KeyCode.E))
        {
            SpeakTo();
        }
    }

    public void SpeakTo()
    {
        

        // Determine which dialogue to start based on the actor's properties or methods
        Dialogue selectedDialogue = GetSelectedDialogue();

        if (selectedDialogue != null)
        {
            DialogueManager.Instance.StartDialogue(Name, selectedDialogue.RootNode);
            interactionText.gameObject.SetActive(false);
            
        }
        else
        {
           
        }
    }
    private Dialogue GetSelectedDialogue()
    {
        // Check if the last selected option is "Spare" for the boss dialogue
        
        if (this.CompareTag("boss") && bossController.HP == 0)
        {
            if (Dialogues.Count > 0)
            {
                bossController.isDead = true;
                return Dialogues[0];
                
            }
        }
        
        if (Dialogues.Count > 0 && !this.CompareTag("boss"))
        {
            return Dialogues[0];
        }
        else
        {
            return null;

        }

        
    }
}
