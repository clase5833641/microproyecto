using UnityEngine;

public class Turret : MonoBehaviour
{
    public GameObject[] objectPrefabs; 
    public Transform[] startPositions; 
    public Transform[] endPositions; 
    public float speed = 7f; 
    public float spawnInterval = 2f;
    public AudioSource firing;

    void Start()
    {
       
        InvokeRepeating("SpawnObject", 0f, spawnInterval);
    }

    void SpawnObject()
    {
        
        int randomStartIndex = Random.Range(0, startPositions.Length);
        Transform startPos = startPositions[randomStartIndex];
        Transform endPos = endPositions[randomStartIndex];

      
        GameObject selectedPrefab = objectPrefabs[Random.Range(0, objectPrefabs.Length)];

       
        GameObject spawnedObject = Instantiate(selectedPrefab, startPos.position, Quaternion.identity);
        if (firing != null) 
        {
            firing.Play();
        }

    
        Vector3 direction = (endPos.position - startPos.position).normalized;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        spawnedObject.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

   
        StartCoroutine(MoveObject(spawnedObject.transform, endPos.position));
    }

    System.Collections.IEnumerator MoveObject(Transform objTransform, Vector3 targetPosition)
    {
        while (objTransform.position != targetPosition)
        {
          
            objTransform.position = Vector3.MoveTowards(objTransform.position, targetPosition, speed * Time.deltaTime);
            yield return null;
        }

    
        Destroy(objTransform.gameObject);
    }
}
