using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class PortalScript : MonoBehaviour
{

    public GameObject Player;
    public GameObject PortalExit;
    public TMP_Text interactionText;
    private bool canInteract = false;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (canInteract && Input.GetKeyDown(KeyCode.E))
        {
            Teleport();
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            interactionText.gameObject.SetActive(true);
            interactionText.text = "Press E to interact";
            canInteract = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            interactionText.gameObject.SetActive(false);
            canInteract = false;
        }
    }
    private void Teleport()
    {
        Player.transform.position = PortalExit.transform.position;
    }

}
