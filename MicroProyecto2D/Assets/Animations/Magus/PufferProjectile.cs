using UnityEngine;

public class PufferProjectile : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float stoppingDistance = 2f;

    private bool movingStraight = false;
    private Transform player;
    private Vector3 lastDirection;

    public void SetMovingStraight(bool value)
    {
        movingStraight = value;
        if (value && player != null)
        {
           
            lastDirection = (player.position - transform.position).normalized;
        }
    }

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player")?.transform;
    }

    private void Update()
    {
        if (!movingStraight)
        {
            
            if (player != null)
            {
                Vector3 playerDirection = (player.position - transform.position).normalized;
                transform.position += playerDirection * Time.deltaTime * moveSpeed;

               
                if (Vector3.Distance(transform.position, player.position) <= stoppingDistance)
                {
                    
                    SetMovingStraight(true);
                }
            }
        }
        else
        {
           
            transform.position += lastDirection * Time.deltaTime * moveSpeed;
        }
    }
}
