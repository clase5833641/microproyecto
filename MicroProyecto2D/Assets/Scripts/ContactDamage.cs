using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactDamage : MonoBehaviour
{
    private GameObject player;

    private int damage = 1000;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        
        if ((collider.GetComponent<PlayerHealth>() != null) && (collider.CompareTag("Player")))
        {
            PlayerHealth health = collider.GetComponent<PlayerHealth>();
            if (health != null)
            {
                health.Damage(damage);
            }
            
        }    
    }
    private void OnDestroy()
    {
        player = null;
    }
}
