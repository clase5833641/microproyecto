using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostEnemy : MonoBehaviour
{
    private bool isTargetingPlayer = true;
    public float speed = 3f;
    public float detectionRange = 5f;
    public Transform player;
    private Rigidbody2D rb;
    private SpriteRenderer spriteRenderer;
    private bool isFacingRight = true;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    void Update()
    {
        if (player  != null) 
        {
            float distanceToPlayer = Vector2.Distance(transform.position, player.position);

            if ((distanceToPlayer <= detectionRange) && isTargetingPlayer)
            {
                detectionRange += 50;
                Vector2 direction = player.position - transform.position;
                direction.Normalize();
                rb.velocity = direction * speed;

                if (direction.x > 0 && !isFacingRight)
                {
                    Flip();
                }
                else if (direction.x < 0 && isFacingRight)
                {
                    Flip();
                }
            }
            else
            {
                rb.velocity = Vector2.zero;
            }
        }
        
    }


    private void OnDestroy()
    {
        player = null;
    }

    void Flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    public void StopTargetingPlayer()
    {
        isTargetingPlayer = false;
    }
}
