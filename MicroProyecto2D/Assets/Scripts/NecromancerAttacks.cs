using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public SpawnAndMoveObject attack1;
    public SkeletonSpawn attack2;
    public GameObject triggerArea;
    public GameObject activatableObject;
    public GameObject blinkObject; 

    private float attackTimer = 6f;
    private float timer = 0f;
    private bool playerEntered = false;
    private bool isFirstAttack = true;
    public Animator animator;
    private bool isBlinking = false;
    private bool blinkState = false;
    private float blinkInterval = 0.1f;
    void Start()
    {
        TriggerAreaScript triggerAreaScript = triggerArea.GetComponent<TriggerAreaScript>();
        if (triggerAreaScript != null)
        {
            triggerAreaScript.SetEnemyController(this);
        }
        else
        {
            Debug.LogError("TriggerAreaScript not found on the triggerArea GameObject.");
        }
    }

    void Update()
    {
        if (playerEntered)
        {
            if (isFirstAttack)
            {
                blinkObject.SetActive(true);
                attack1.StartSpawningProcess();
                isFirstAttack = false;
            }
            else
            {
                animator.ResetTrigger("IsAttacking");
                timer += Time.deltaTime;

               
                if (timer >= attackTimer - 0.5f && !isBlinking)
                {
                    isBlinking = true;
                    InvokeRepeating("BlinkObject", 0f, blinkInterval);
                }

                if (timer >= attackTimer)
                {
                    timer = 0f;

                    int randomAttack = Random.Range(1, 3);
                    if (randomAttack == 1)
                    {
                        attack1.StartSpawningProcess();
                    }
                    else
                    {
                        attack2.ExecuteAttack();
                    }

                    if (isBlinking)
                    {
                        CancelInvoke("BlinkObject");
                        blinkObject.SetActive(true);
                        isBlinking = false;
                    }
                }
            }
        }
    }


    void BlinkObject()
    {
        blinkObject.SetActive(blinkState);
        blinkState = !blinkState;
    }

    public void PlayerEnteredTriggerArea()
    {
        playerEntered = true;
        activatableObject.SetActive(true);
    }

    public void PlayerExitedTriggerArea()
    {
        playerEntered = false;
        isFirstAttack = true;

 
        if (isBlinking)
        {
            CancelInvoke("BlinkObject");
            blinkObject.SetActive(true); 
            isBlinking = false;
        }
    }

    void OnDestroy()
    {
        if (activatableObject != null)
        {
            activatableObject.SetActive(false);
        }
    }
}
