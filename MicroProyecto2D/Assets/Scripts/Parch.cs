using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parch : MonoBehaviour
{
    public int value;
    public AudioSource pickup;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D other)
         
    {
        if (other.CompareTag("Player"))
        {
            if (pickup != null) 
            {
                pickup.Play();
            }
            Destroy(gameObject);
            ParchManager.instance.IncreaseParch(value);
        }
    }
}
