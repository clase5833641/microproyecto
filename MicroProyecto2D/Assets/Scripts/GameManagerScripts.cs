using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScripts : MonoBehaviour
{
    private GameObject player;
    public GameObject gameOverUI;
    public GameObject boss1;
    public GameObject respawnButton;
    public Vector3 respawnPosition;
    private bool hasReachedCheckpoint = false;
    private Vector3 lastCheckpointPosition;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        respawnButton.SetActive(false); 
        
        lastCheckpointPosition = player.transform.position;
    }

    public void SetCheckpoint(Vector3 position)
    {
        lastCheckpointPosition = position;
        hasReachedCheckpoint = true;
        respawnButton.SetActive(true); 
      
    }

    public void RespawnAtLastCheckpoint()
    {
        GameObject currentBoss = GameObject.FindGameObjectWithTag("boss");
        if (currentBoss != null) 
        {
            Destroy(currentBoss);
        }
        Instantiate(boss1, respawnPosition, Quaternion.identity);
        player.transform.position = lastCheckpointPosition;
       
        player.SetActive(true); 
        gameOverUI.SetActive(false);
    }

    public void GameOver()
    {
        gameOverUI.SetActive(true);
    }
    public void Exit()
    {
        Application.Quit();
    }

    public void Restart()
    {
       
        hasReachedCheckpoint = false;
       
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void RespawnAtCheckpointButton()
    {
        if (hasReachedCheckpoint)
        {
            RespawnAtLastCheckpoint();
            respawnButton.SetActive(false); 
        }
    }
}
