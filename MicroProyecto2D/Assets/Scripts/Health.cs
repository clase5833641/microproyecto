using System.Collections;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private int health;
    [SerializeField] private int maxHP;
    private bool canTakeDamage = true;
    private SpriteRenderer spriteRenderer; // Reference to the SpriteRenderer component

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>(); // Get the SpriteRenderer component
    }

    void Update()
    {

    }

    public void Damage(int amount)
    {
        if (amount < 0)
        {
            throw new System.ArgumentOutOfRangeException("cant have negative health");
        }

        if (canTakeDamage)
        {
            this.health -= amount;

            if (this.health <= 0)
            {
                Die();
            }
            else
            {
                StartCoroutine(DamageCooldown(0.5f));
                StartCoroutine(BlinkSprite(0.2f, 0.05f)); // Blink the sprite for a short duration with a fast speed
            }
        }
    }

    public void Heal(int amount)
    {
        if (amount < 0)
        {
            throw new System.ArgumentOutOfRangeException("cant have negative healing");
        }

        if (health + amount > maxHP)
        {
            this.health = maxHP;
        }
        else
        {
            this.health += amount;
        }
    }

    private IEnumerator DamageCooldown(float cooldownTime)
    {
        canTakeDamage = false;
        yield return new WaitForSeconds(cooldownTime);
        canTakeDamage = true;
    }

    private IEnumerator BlinkSprite(float duration, float blinkInterval)
    {
        float endTime = Time.time + duration;
        while (Time.time < endTime)
        {
            spriteRenderer.enabled = !spriteRenderer.enabled; 
            yield return new WaitForSeconds(blinkInterval);
        }
        spriteRenderer.enabled = true; 
    }

    private void Die()
    {
        Destroy(gameObject);
    }
}
