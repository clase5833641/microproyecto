using System.Collections;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject objectPrefab;
    public Transform spawnPosition;
    public Transform targetPosition;
    public float movementSpeed = 5f;
    public float spawnInterval = 2f;
    public GameObject door3;

    private Vector3 originalPosition;
    private bool canSpawn = false;

    private void Start()
    {
        originalPosition = spawnPosition.position;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            canSpawn = true;
            InvokeRepeating(nameof(SpawnObject), 0f, spawnInterval);
        }
        door3.SetActive(true);
    }

    

    private void SpawnObject()
    {
        if (canSpawn)
        {
            GameObject spawnedObject = Instantiate(objectPrefab, spawnPosition.position, Quaternion.identity);
            MoveObject(spawnedObject);
        }
    }

    private void MoveObject(GameObject obj)
    {
        StartCoroutine(MoveRoutine(obj));
    }

    private IEnumerator MoveRoutine(GameObject obj)
    {
        Vector3 currentTarget = targetPosition.position;
        while (Vector3.Distance(obj.transform.position, currentTarget) > 0.1f)
        {
            obj.transform.position = Vector3.MoveTowards(obj.transform.position, currentTarget, movementSpeed * Time.deltaTime);
            yield return null;
        }

        if (CheckCollisionWithTag(obj, "hitBox"))
        {
            currentTarget = originalPosition;
            StartCoroutine(MoveRoutine(obj));
        }
        else if (CheckCollisionWithTag(obj, "enemy"))
        {
            Destroy(obj);
        }
    }

    private bool CheckCollisionWithTag(GameObject obj, string tag)
    {
        Collider2D[] colliders = Physics2D.OverlapBoxAll(obj.transform.position, obj.GetComponent<Collider2D>().bounds.extents, 0f);
        foreach (Collider2D collider in colliders)
        {
            if (collider.CompareTag(tag))
            {
                return true;
            }
        }
        return false;
    }
}
