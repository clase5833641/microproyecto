using UnityEngine;

public class TennisBall : MonoBehaviour
{
    public float pspeed = 5f;
    public Vector2 initialPosition; // The initial position where the projectile spawns
    public Vector2 initialTargetPosition; // The initial target position the projectile moves towards
    public string enemyTag = "enemy"; // Tag of the enemy object
    public string changeTargetTag = "hitBox"; // Tag of the object to change target position
    public int damageAmount = 1; // Amount of damage the projectile deals

    private Vector2 currentTargetPosition; // Current target position the projectile is moving towards

    void Start()
    {
        transform.position = initialPosition; // Set the initial position
        currentTargetPosition = initialTargetPosition; // Set the initial target position
        MoveToTarget();
    }

    void Update()
    {
        // If the projectile reaches its initial target position or collides with an enemy object, destroy it
        if (Vector2.Distance(transform.position, initialTargetPosition) < 0.1f)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(enemyTag))
        {
            Health enemyHealth = collision.GetComponent<Health>();
            if (enemyHealth != null)
            {
                enemyHealth.Damage(damageAmount);
            }
            Destroy(gameObject);
        }
        else if (collision.CompareTag(changeTargetTag))
        {
            // If the projectile collides with an object tagged with "ChangeTarget", update the target position
            currentTargetPosition = initialPosition;
            MoveToTarget(); // Move back to the initial position
        }
    }

    // Move the projectile towards the current target position
    void MoveToTarget()
    {
        Vector2 direction = (currentTargetPosition - (Vector2)transform.position).normalized;
        GetComponent<Rigidbody2D>().velocity = direction * pspeed;
    }
}
