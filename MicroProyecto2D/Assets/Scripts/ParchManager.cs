using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ParchManager : MonoBehaviour
{
    public static ParchManager instance;
    public TMP_Text parchText;
    public TMP_Text message;
    public int parchCounter = 0;
    public GameObject lichenemies;
    public GameObject portalbot;
    public GameObject portaltot;
    private bool messagesent = false;
    // Start is called before the first frame update

    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        parchText.text = "Lines of Code = " + parchCounter.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (parchCounter  >= 4 && lichenemies == null) 
        {

            portalbot.SetActive(true);
            portaltot.SetActive(true);

        }
        if (parchCounter < 4 && lichenemies == null && !messagesent)
        {
            messagesent = true;
            message.text = "looks like i still need more lines of code to climb there...";
            StartCoroutine (Mensaje());
        }
    }

    IEnumerator Mensaje ()
    {
        yield return new WaitForSeconds(5f);
        message.text = "";
    }
    public void IncreaseParch(int v)
    {
        parchCounter += v;
        parchText.text = "Lines of Code = " + parchCounter.ToString();
    }
}
