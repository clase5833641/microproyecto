using UnityEngine;

public class TriggerAreaScript : MonoBehaviour
{
    private EnemyController enemyController;

    public void SetEnemyController(EnemyController controller)
    {
        enemyController = controller;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && enemyController != null)
        {
            enemyController.PlayerEnteredTriggerArea();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player") && enemyController != null)
        {
            enemyController.PlayerExitedTriggerArea();
        }
    }
}
