using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevelScript : MonoBehaviour
{
    public GameObject Player;
    public GameObject boss1;
    public GameObject NextLevel;
    public TMP_Text interactionText;
    private bool hasReachedCheckpoint = false;
    private Vector3 lastCheckpointPosition;
    private bool canInteract = false;
    public GameObject finalscreen;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (boss1 == null)
        {
            NextLevel.SetActive (true); 
        }
        if (canInteract && Input.GetKeyDown(KeyCode.E))
        {
            final();
        }
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            interactionText.gameObject.SetActive(true);
            interactionText.text = "Press E to interact";
            canInteract = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (interactionText != null) 
            { interactionText.gameObject.SetActive(false); }
            
            canInteract = false;
        }
    }
    private void final()
    {
        finalscreen.SetActive(true);
    }

    public void Restart()
    {
      
        hasReachedCheckpoint = false;
    
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void Quit()
    {
        Application.Quit();
    }
}
