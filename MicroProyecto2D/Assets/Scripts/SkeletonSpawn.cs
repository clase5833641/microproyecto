using UnityEngine;

public class SkeletonSpawn : MonoBehaviour
{
    public GameObject objectPrefab;
    public float moveSpeed = 5f;
    public Transform player;
    public float spawnXPosition;
    public float fixedYPosition;
    public float destroyDelay = 4.5f;

    private bool facingRight = true;
    private GameObject spawnedObject;
    public Animator enemyAnimator;
    public AudioSource laugh1;
    public AudioSource laugh2;

    void Start()
    {
        
        
    }

    public void ExecuteAttack()
    {


        enemyAnimator.SetBool("IsAttacking", true);
        spawnedObject = Instantiate(objectPrefab, new Vector3(spawnXPosition, fixedYPosition, 0f), Quaternion.identity);
        int randomNumber = Random.Range(1, 3);
        switch (randomNumber)
        {
            case 1:
                if (laugh1 != null)
                {
                    laugh1.Play();
                }
                break;
            case 2:
                if (laugh2 != null)
                {
                    laugh2.Play();
                }
                break;
        }


        Vector3 direction = (player.position - spawnedObject.transform.position).normalized;
        Rigidbody2D rb = spawnedObject.GetComponent<Rigidbody2D>();
        rb.velocity = direction * moveSpeed;

        
        if (player.position.x < spawnedObject.transform.position.x && facingRight)
        {
            Flip(spawnedObject);
        }
        else if (player.position.x > spawnedObject.transform.position.x && !facingRight)
        {
            Flip(spawnedObject);
        }

        
        Invoke("DestroySpawnedObject", destroyDelay);
    }

    private void Flip(GameObject objToFlip)
    {
        
        facingRight = !facingRight;
        Vector3 scale = objToFlip.transform.localScale;
        scale.x *= -1;
        objToFlip.transform.localScale = scale;
    }

    private void DestroySpawnedObject()
    {
        
        Destroy(spawnedObject);
    }
}
