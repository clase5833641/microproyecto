using UnityEngine;


public class Projectile : MonoBehaviour
{
    public float speed = 10f;
    public float cooldownTime = 2f;

    private Rigidbody2D rb;
    private SpriteRenderer playerSpriteRenderer;
    public SpriteRenderer objectSpriteRenderer;
    private bool canShoot = true;
    private float cooldownTimer = 0f;
    private bool fired = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        playerSpriteRenderer = GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        cooldownTimer -= Time.deltaTime;

        if (cooldownTimer <= 0 && canShoot && !fired)
        {
            Vector2 direction = playerSpriteRenderer.flipX ? Vector2.left : Vector2.right;
            if (playerSpriteRenderer.flipX == true)
            {
                objectSpriteRenderer.flipX = true;
            }
            rb.velocity = direction * speed;
            fired = true;
        }
        else if (!fired)
        {
            rb.velocity = Vector2.zero;
        }
    }

    void Shoot()
    {
        cooldownTimer = cooldownTime;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("enemy"))
        {
            Health enemyHealth = other.GetComponent<Health>();
            if (enemyHealth != null)
            {
                enemyHealth.Damage(1);
            }
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }
}