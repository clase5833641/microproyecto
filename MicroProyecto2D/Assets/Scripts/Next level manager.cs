using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nextlevelmanager : MonoBehaviour
{

    public GameObject boss1;
    public GameObject nextLevelDoor;
    // Start is called before the first frame update
    void Start()
    {
        nextLevelDoor.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (boss1 == null)
        {
            nextLevelDoor.SetActive(true);
        }
    }
}
