using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManagerScripts gameManager = FindObjectOfType<GameManagerScripts>();
            gameManager.SetCheckpoint(transform.position);
            // You can add more logic here such as displaying a message indicating the checkpoint is reached
        }
    }
}
