using System;
using System.Runtime.CompilerServices;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

namespace TarodevController
{
    /// <summary>
    /// Hey!
    /// Tarodev here. I built this controller as there was a severe lack of quality & free 2D controllers out there.
    /// I have a premium version on Patreon, which has every feature you'd expect from a polished controller. Link: https://www.patreon.com/tarodev
    /// You can play and compete for best times here: https://tarodev.itch.io/extended-ultimate-2d-controller
    /// If you hve any questions or would like to brag about your score, come to discord: https://discord.gg/tarodev
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
    public class PlayerController : MonoBehaviour, IPlayerController
    {
        [SerializeField] private ScriptableStats _stats;
        private bool _isDialogueActive = false; // Flag to check if dialogue is active

        private Rigidbody2D _rb;
        private GameManagerScripts gameManager;
        private CapsuleCollider2D _col;
        private FrameInput _frameInput;
        private Vector2 _frameVelocity;
        private bool _cachedQueryStartInColliders;
        private SpriteRenderer sprite;
        public GameObject projectilePrefab;
        [SerializeField] private float spawnOffset = 1.5f;
        public float shootForce = 10f; 
        public float cooldown = 1.5f; 
        private float cooldownTimer = 0f;
        public Transform firePoint;
        private float lastFireTime = 0f;
        private Animator animator;
        public float walkThreshold = 0.1f;
        public float jumpThreshold = 2500f;
        public float fallThreshold = -0.5f;
        public Transform groundCheck;
        public LayerMask groundLayer;
        public float groundCheckRadius = 0.1f;
        private bool isGrounded;
        private float facingDirection = 1f;
        private bool playerIsFacingRight = true;
        private Projectile projectile;
        bool isFacingRight = true;
        private bool isFunctionCalled = false;
        private float lastCalledTime = 0f;
        public GameObject hitBox;
        public float radius;
        public LayerMask enemies;
        public int damageAmount = 1;
        public LayerMask triggerAreaLayer;
        public float pushForce = 10f;
        public GameObject magus;
        public AudioSource attackSound;
        public AudioSource shootally;
        public AudioSource jump;
        private bool isAttackCooldown = false; // Flag to track if attack is on cooldown
        private float attackCooldownDuration = 0.5f; // Cooldown duration in seconds
        private float attackCooldownTimer = 0f;
        private bool hasPlayedSound = false;
        #region Interface

        public Vector2 FrameInput => _frameInput.Move;
        public event Action<bool, float> GroundedChanged;
        public event Action Jumped;

        #endregion

        private float _time;

        private void Awake()
        {
            magus = GameObject.Find("magus");

            animator = GetComponent<Animator>();
            _rb = GetComponent<Rigidbody2D>();
            _col = GetComponent<CapsuleCollider2D>();
            

            sprite = GetComponent<SpriteRenderer>();

            _cachedQueryStartInColliders = Physics2D.queriesStartInColliders;
        }
        private void Start()
        {
            gameManager = FindObjectOfType<GameManagerScripts>();
        }
        private void Update()
        {
            if (!_isDialogueActive)
            {
                float moveInput = Input.GetAxisRaw("Horizontal");
                isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);

                UpdateAnimatorParameters();

                _time += Time.deltaTime;
                GatherInput();
                cooldownTimer -= Time.deltaTime;

                isFacingRight = _frameInput.Move.x > 0;

                FlipFirePoint(moveInput);

                if (Time.time - lastCalledTime >= 2f)
                {
                    isFunctionCalled = false;
                }

                if (Input.GetKeyDown(KeyCode.P))
                {
                    Shoot();

                }
                if (isAttackCooldown)
                {
                    attackCooldownTimer -= Time.deltaTime;
                    if (attackCooldownTimer <= 0f)
                    {
                        isAttackCooldown = false; // Reset cooldown flag when cooldown period ends
                    }
                }
                // Allow attacking while jumping
                if (!isAttackCooldown && Input.GetKeyDown(KeyCode.L) && (isGrounded || _frameInput.JumpHeld))
                {
                    Attack();
                }
            }
        }


        private void UpdateAnimatorParameters()
        {
            bool isWalking = Mathf.Abs(_rb.velocity.x) > walkThreshold && isGrounded;
            bool isJumping = _rb.velocity.y > jumpThreshold && !isGrounded;
            bool isFalling = _rb.velocity.y < fallThreshold && !isGrounded;
            bool isAttacking = Input.GetKeyDown(KeyCode.L);
            
            bool isJumpAttacking = isAttacking && !isGrounded; // New parameter for jumping attack

            animator.SetBool("IsWalking", isWalking);
            animator.SetBool("IsJumping", isJumping);
            animator.SetBool("IsFalling", isFalling);
            animator.SetBool("IsAttacking", isAttacking);
            animator.SetBool("IsJumpAttacking", isJumpAttacking); // Set the new parameter
        }

        public void DisableInput()
        {
            enabled = false;
        }

        public void EnableInput()
        {
            enabled = true;
        }


        private void ActivateHitBox()
        {
            
            hitBox.SetActive(true);
            if (attackSound != null)
            {
                attackSound.Play();
            }
        }
        public void Attack()
        {
            ActivateHitBox();
            

            // Play the attack sound
            

            // Start the attack cooldown
            isAttackCooldown = true;
            attackCooldownTimer = attackCooldownDuration;

            Collider2D[] enemiesInRange = Physics2D.OverlapCircleAll(hitBox.transform.position, radius, enemies);
            foreach (Collider2D enemyCollider in enemiesInRange)
            {
                Health enemyHealth = enemyCollider.GetComponent<Health>();
                if (enemyHealth != null)
                {
                    enemyHealth.Damage(damageAmount);
                }
                BossController otherScript = magus.GetComponent<BossController>();
                if (otherScript != null)
                {

                    otherScript.hitCounter += 1;
                }

            }
            foreach (Collider2D enemyCollider in enemiesInRange)
            {
                if (enemyCollider.CompareTag("lich"))
                {
                    if (_rb != null)
                    {

                        Vector2 pushDirection = (transform.position - enemyCollider.transform.position).normalized;


                        _rb.AddForce(pushDirection * pushForce, ForceMode2D.Impulse);
                    }
                }
            }
            hasPlayedSound = false;

        }



        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(hitBox.transform.position, radius);
        }
        void OnTriggerEnter2D (Collider2D other)
        {
            if (other.CompareTag("Checkpoint"))
            {
                gameManager.SetCheckpoint(other.transform.position);
                // You can add more logic here such as displaying a message indicating the checkpoint is reached
            }
            // Add other collision logic if needed
        }


        private void GatherInput()
        {
            _frameInput = new FrameInput
            {
                JumpDown = Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.C),
                JumpHeld = Input.GetButton("Jump") || Input.GetKey(KeyCode.C),
                Move = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"))
            };

            if (_frameInput.Move.x > 0)
            {
                sprite.flipX = false;
               
            }
            else if(_frameInput.Move.x < 0) {
                sprite.flipX = true;
                

                
            }

            if (_stats.SnapInput)
            {
                _frameInput.Move.x = Mathf.Abs(_frameInput.Move.x) < _stats.HorizontalDeadZoneThreshold ? 0 : Mathf.Sign(_frameInput.Move.x);
                _frameInput.Move.y = Mathf.Abs(_frameInput.Move.y) < _stats.VerticalDeadZoneThreshold ? 0 : Mathf.Sign(_frameInput.Move.y);
            }

            if (_frameInput.JumpDown)
            {
                _jumpToConsume = true;
                _timeJumpWasPressed = _time;
            }
        }

        void FlipFirePoint(float direction)
        {
            if (direction != 0)
            {
                
                if (direction > 0)
                {

                    hitBox.transform.localPosition = firePoint.localPosition = new Vector3(Mathf.Abs(firePoint.localPosition.x), firePoint.localPosition.y, firePoint.localPosition.z);
                   

                }
                else 
                {

                    hitBox.transform.localPosition = firePoint.localPosition = new Vector3(-Mathf.Abs(firePoint.localPosition.x), firePoint.localPosition.y, firePoint.localPosition.z);
                   
                    
                }
            }
        }

        private void FixedUpdate()
        {
            CheckCollisions();

            HandleJump();
            HandleDirection();
            HandleGravity();
            
            ApplyMovement();
        }

        #region Collisions
        
        private float _frameLeftGrounded = float.MinValue;
        private bool _grounded;

        void Shoot()
        {
            if (!isFunctionCalled)
            {
                if (shootally != null)
                {
                    shootally.Play();
                }
                GameObject newProjectile = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
                Projectile projectileScript = newProjectile.GetComponent<Projectile>();
                isFunctionCalled = true;
                lastCalledTime = Time.time;
            }

        }
        private void CheckCollisions()
        {
            Physics2D.queriesStartInColliders = false;

            // Ground and Ceiling
            bool groundHit = Physics2D.CapsuleCast(_col.bounds.center, _col.size, _col.direction, 0, Vector2.down, _stats.GrounderDistance, ~_stats.PlayerLayer);
            bool ceilingHit = Physics2D.CapsuleCast(_col.bounds.center, _col.size, _col.direction, 0, Vector2.up, _stats.GrounderDistance, ~_stats.PlayerLayer);

            // Hit a Ceiling
            if (ceilingHit) _frameVelocity.y = Mathf.Min(0, _frameVelocity.y);

            // Landed on the Ground
            if (!_grounded && groundHit)
            {
                _grounded = true;
                _coyoteUsable = true;
                _bufferedJumpUsable = true;
                _endedJumpEarly = false;
                GroundedChanged?.Invoke(true, Mathf.Abs(_frameVelocity.y));
            }
            // Left the Ground
            else if (_grounded && !groundHit)
            {
                _grounded = false;
                _frameLeftGrounded = _time;
                GroundedChanged?.Invoke(false, 0);
            }

            Physics2D.queriesStartInColliders = _cachedQueryStartInColliders;
        }

        #endregion


        #region Jumping

        private bool _jumpToConsume;
        private bool _bufferedJumpUsable;
        private bool _endedJumpEarly;
        private bool _coyoteUsable;
        private float _timeJumpWasPressed;

        private bool HasBufferedJump => _bufferedJumpUsable && _time < _timeJumpWasPressed + _stats.JumpBuffer;
        private bool CanUseCoyote => _coyoteUsable && !_grounded && _time < _frameLeftGrounded + _stats.CoyoteTime;

        private void HandleJump()
        {
            if (!_endedJumpEarly && !_grounded && !_frameInput.JumpHeld && _rb.velocity.y > 0) _endedJumpEarly = true;

            if (!_jumpToConsume && !HasBufferedJump) return;

            if (_grounded || CanUseCoyote) ExecuteJump();

            _jumpToConsume = false;
        }

        private void ExecuteJump()
        {
            if (jump != null)
            {
                jump.Play();
            }
            _endedJumpEarly = false;
            _timeJumpWasPressed = 0;
            _bufferedJumpUsable = false;
            _coyoteUsable = false;
            _frameVelocity.y = _stats.JumpPower;
            Jumped?.Invoke();
        }

        #endregion

        #region Horizontal

        private void HandleDirection()
        {
            if (_frameInput.Move.x == 0)
            {
                var deceleration = _grounded ? _stats.GroundDeceleration : _stats.AirDeceleration;
                _frameVelocity.x = Mathf.MoveTowards(_frameVelocity.x, 0, deceleration * Time.fixedDeltaTime);
            }
            else
            {
                
                _frameVelocity.x = Mathf.MoveTowards(_frameVelocity.x, _frameInput.Move.x * _stats.MaxSpeed, _stats.Acceleration * Time.fixedDeltaTime);


            }
        }

        #endregion

        #region Gravity

        private void HandleGravity()
        {
            if (_grounded && _frameVelocity.y <= 0f)
            {
                _frameVelocity.y = _stats.GroundingForce;
            }
            else
            {
                var inAirGravity = _stats.FallAcceleration;
                if (_endedJumpEarly && _frameVelocity.y > 0) inAirGravity *= _stats.JumpEndEarlyGravityModifier;
                _frameVelocity.y = Mathf.MoveTowards(_frameVelocity.y, -_stats.MaxFallSpeed, inAirGravity * Time.fixedDeltaTime);
            }
        }

        #endregion

        private void ApplyMovement() => _rb.velocity = _frameVelocity;

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (_stats == null) Debug.LogWarning("Please assign a ScriptableStats asset to the Player Controller's Stats slot", this);
        }
#endif
    }

    public struct FrameInput
    {
        public bool JumpDown;
        public bool JumpHeld;
        public Vector2 Move;
    }

    public interface IPlayerController
    {
        public event Action<bool, float> GroundedChanged;

        public event Action Jumped;
        public Vector2 FrameInput { get; }
    }

    
}