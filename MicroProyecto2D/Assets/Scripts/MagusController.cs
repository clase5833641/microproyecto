using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

public class BossController : MonoBehaviour
{
    private float delayBetweenProjectiles = 0.5f;
    public float HP = 2;
    public float hitCounter = 0;
    public Image hp3;
    public Image hp2;
    public Image hp1;
    public Image hp0;
    public Transform destinationTransform;
    public Transform attack1Transform;
    public Transform attack2Transform;
    public Transform attack3Transform;
    public Transform dpsTransform;
    private SpriteRenderer spriteRenderer;
    private int attackCount;
    private bool isResting;
    public GameObject triggerArea;
    private Animator animator;
    private GameObject spawnedObject;
    public GameObject objToSpawn;
    [SerializeField] private float fixedYPosition;
    [SerializeField] private float spawnXPosition;
    public Transform player;
    public float moveSpeed = 5f;
    public GameObject magusDoor;
    public GameObject swipePlatforms;
    private bool playerEntered = false;
    [SerializeField] private float puffer1YPosition;
    [SerializeField] private float puffer1XPosition;
    [SerializeField] private float puffer2YPosition;
    [SerializeField] private float puffer2XPosition;
    [SerializeField] private float puffer3YPosition;
    [SerializeField] private float puffer3XPosition;
    public GameObject pufferBarrage;
    private int shotCounter;
    
    public GameObject[] objectPrefabs;
    public Transform[] startPositions;
    public Transform[] endPositions;
    public float speed = 7f;
    private int atkmuber;
    private bool dpsAvailable = false;
    public bool isDead;
    public bool isSpared;
    private bool fightstarted = false;
    private bool lockAnimation = false;
    private bool isSpawning = false; 
    public float dpsPhaseDuration = 2f; 
    private bool inDPSPhase = false; 
    private float dpsPhaseTimer = 0f; 
    private Collider2D colli;
    public AudioSource fireball;
    public AudioSource swipe;
    public AudioSource snap;
    void Start()
    {
        hp1.gameObject.SetActive(false);
        hp2.gameObject.SetActive(false);
        hp3.gameObject.SetActive(false);
        colli = GetComponent<Collider2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        colli.enabled = false;
        TriggerAreaMagus triggerAreaScript = triggerArea.GetComponent<TriggerAreaMagus>();
        if (triggerAreaScript != null)
        {
            triggerAreaScript.SetBossController(this);
        }

        attackCount = 0;
    }
    private void FixedUpdate()
    {
        if (dpsAvailable)
        {
            
            Collider2D[] collisions = Physics2D.OverlapBoxAll(transform.position, transform.localScale, 0f, LayerMask.GetMask("hitBox"));
            if (collisions.Length > 0)
            {
                
                HP--;
                dpsAvailable = false;
                Debug.Log("Collision detected with SpecificObject");
            }

            
        }
        
    }
    private void Update()
    {
        if (HP <= 0)
        {
            magusDoor.SetActive(false);
            StopAllCoroutines();
            this.transform.position = dpsTransform.transform.position;
            colli.enabled = true;
        }
        if (isDead) { Destroy(gameObject); }
        if (HP == 3 && fightstarted)
        {
            hp3.gameObject.SetActive(true);
            hp2.gameObject.SetActive(false);
            hp1.gameObject.SetActive(false);
        }
        if (HP == 2 && fightstarted)
        {
            hp2.gameObject.SetActive(true);
            hp3.gameObject.SetActive(false);
            hp1.gameObject.SetActive(false);
        }
        if (HP == 1 && fightstarted)
        {
            hp1.gameObject.SetActive(true);
            hp2.gameObject.SetActive(false);
            hp3.gameObject.SetActive(false);
        }
        if (HP == 0 && fightstarted)
        {
            hp1.gameObject.SetActive(false);
            hp2.gameObject.SetActive(false);
            hp3.gameObject.SetActive(false);
            fightstarted = false;
        }
    }

    public void HpBar()
    {
        if (HP == 3 && fightstarted)
        {
            hp3.gameObject.SetActive(true);
            hp2.gameObject.SetActive(false);
            hp1.gameObject.SetActive(false);
        }
        if (HP == 2 && fightstarted)
        {
            hp2.gameObject.SetActive(true);
            hp3.gameObject.SetActive(false);
            hp1.gameObject.SetActive(false);
        }
        if (HP == 1 && fightstarted)
        {
            hp1.gameObject.SetActive(true);
            hp2.gameObject.SetActive(false);
            hp3.gameObject.SetActive(false);
        }
        if (HP == 0 && fightstarted)
        {
            hp1.gameObject.SetActive(false);
            hp2.gameObject.SetActive(false);
            hp3.gameObject.SetActive(false);
            fightstarted = false;
        }

    }
    public void TriggerBossAttack()
    {
        HpBar();
        Debug.Log("Triggering boss attack");
        StartCoroutine(WaitForBossAttack());
    }

    IEnumerator WaitForBossAttack()
    {
        yield return new WaitForSeconds(2f);
        if (!isResting)
        {
            ChooseRandomAttack();
        }
    }

    void ChooseRandomAttack()
    {
        fightstarted = true;
        HpBar();
        if (HP > 0)
        {
            if (atkmuber >= 2) 
            {
                StartCoroutine(DpsPhase());
                Debug.Log("dps");
            }
            else
            {
                int randomAttack = Random.Range(1, 4);

                switch (randomAttack)
                {
                    case 1:
                        StartCoroutine(Attack1Coroutine());
                        break;
                    case 2:
                        StartCoroutine(Attack2Coroutine());
                        break;
                    case 3:
                        StartCoroutine(Attack3Coroutine(0.6f));
                        break;
                    default:
                        Debug.LogError("Invalid attack choice!");
                        break;
                }
            }
            
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void LockAnimation()
    {
        lockAnimation = true;
    }

    
    public void UnlockAnimation()
    {
        lockAnimation = false;
    }

    IEnumerator Attack1Coroutine()
    {
        atkmuber++;
        float projectileDuration = 2.2f;
        float elapsedTime = 0f;
        yield return new WaitForSeconds(0.5f);
        spriteRenderer.enabled = false;
        yield return new WaitForSeconds(0.5f);
        if (swipe != null)
        {
            swipe.Play();
        }
        transform.position = attack1Transform.position;
        yield return new WaitForSeconds(0.5f);
        spriteRenderer.enabled = true;
        animator.SetBool("Swipe", true);
        swipePlatforms.SetActive(true);
        yield return new WaitForSeconds(0.95f);
        spawnedObject = Instantiate(objToSpawn, new Vector3(spawnXPosition, fixedYPosition, 0f), Quaternion.identity);
        
        animator.SetBool("Swipe", false);

        Vector3 direction = Vector3.right;
        Rigidbody2D rb = spawnedObject.GetComponent<Rigidbody2D>();
        rb.velocity = direction * moveSpeed;

        while (elapsedTime < projectileDuration)
        {
            elapsedTime += Time.deltaTime;

            if (spawnedObject == null)
                yield break;

            yield return null;
        }

        Destroy(spawnedObject);
        swipePlatforms.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(Teleport());
    }

    IEnumerator Attack2Coroutine()
    {
        atkmuber++;
        lockAnimation = false;
        if (animator != null)
        {
            animator.speed = 1f;
        }
        yield return new WaitForSeconds(0.5f);
        spriteRenderer.enabled = false;
        yield return new WaitForSeconds(0.5f);
        transform.position = attack2Transform.position;
        yield return new WaitForSeconds(0.5f);
        spriteRenderer.enabled = true;
        animator.SetBool("TrackingBarrage", true);
        
        yield return new WaitForSeconds(0.4f);
        if (snap != null)
        {
            snap.Play();
        }
        yield return new WaitForSeconds(0.4f);
        

        
        GameObject pufferBarrage1 = Instantiate(pufferBarrage, new Vector3(puffer1XPosition, puffer1YPosition, 0f), Quaternion.identity);
        PufferProjectile pufferProjectile1 = pufferBarrage1.GetComponent<PufferProjectile>();
        pufferProjectile1.SetMovingStraight(false);

        yield return new WaitForSeconds(delayBetweenProjectiles);

        
        GameObject pufferBarrage2 = Instantiate(pufferBarrage, new Vector3(puffer2XPosition, puffer2YPosition, 0f), Quaternion.identity);
        PufferProjectile pufferProjectile2 = pufferBarrage2.GetComponent<PufferProjectile>();
        pufferProjectile2.SetMovingStraight(false);

        yield return new WaitForSeconds(delayBetweenProjectiles);

        
        GameObject pufferBarrage3 = Instantiate(pufferBarrage, new Vector3(puffer3XPosition, puffer3YPosition, 0f), Quaternion.identity);
        PufferProjectile pufferProjectile3 = pufferBarrage3.GetComponent<PufferProjectile>();
        pufferProjectile3.SetMovingStraight(false);

        yield return new WaitForSeconds(delayBetweenProjectiles);

      
        pufferProjectile1.SetMovingStraight(true);
        pufferProjectile2.SetMovingStraight(true);
        pufferProjectile3.SetMovingStraight(true);
        animator.SetBool("TrackingBarrage", false);
        yield return new WaitForSeconds(1.5f);
        lockAnimation = false;
        if (animator != null)
        {
            animator.speed = 1f;
        }
        StartCoroutine(Teleport());
    }
    IEnumerator Attack3Coroutine(float spawningTimer)
    {
        atkmuber++;

        yield return new WaitForSeconds(0.5f);
        spriteRenderer.enabled = false;
        yield return new WaitForSeconds(0.5f);
        transform.position = attack3Transform.position;
        yield return new WaitForSeconds(0.5f);

        spriteRenderer.enabled = true;
        spriteRenderer.flipX = true;
        animator.SetBool("puffer", true);

        yield return new WaitForSeconds(0.8f);
        isSpawning = true;
        float elapsedTime = 0f; 

       
        spriteRenderer.flipX = true;
       
        
        while (isSpawning && shotCounter <= 10)
        {
            
          
            int randomStartIndex = Random.Range(0, startPositions.Length);
            Transform startPos = startPositions[randomStartIndex];
            Transform endPos = endPositions[randomStartIndex];

           
            GameObject selectedPrefab = objectPrefabs[Random.Range(0, objectPrefabs.Length)];

            lockAnimation = true;
            if (lockAnimation && animator != null)
            {
                animator.Play(animator.GetCurrentAnimatorStateInfo(0).fullPathHash, 0, 1f);
                animator.speed = 0f; 
            }
            GameObject spawnedObject = Instantiate(selectedPrefab, startPos.position, Quaternion.identity);
            if (fireball != null)
            {
                fireball.Play();
            }

          
            SpriteRenderer spawnedSpriteRenderer = spawnedObject.GetComponent<SpriteRenderer>();
            if (spawnedSpriteRenderer != null)
            {
                spawnedSpriteRenderer.flipX = true;
            }

          
            Vector3 movementDirection = endPos.position - startPos.position;
            if (movementDirection.y < 0)
            {
               
                if (spawnedSpriteRenderer != null)
                {
                    spawnedSpriteRenderer.flipY = true;
                }
            }

          
            StartCoroutine(MoveObject(spawnedObject.transform, endPos.position));

            
            shotCounter++;

           
            yield return new WaitForSeconds(spawningTimer);
        }

       
        
        animator.SetBool("puffer", false);
        
        yield return new WaitForSeconds(1.5f);
        lockAnimation = false;
        if (animator != null)
        {
            animator.speed = 1f;
        }
        
        StartCoroutine(Teleport());
        
    }

    IEnumerator MoveObject(Transform objTransform, Vector3 targetPosition)
    {
        while (objTransform.position != targetPosition)
        {
          
            objTransform.position = Vector3.MoveTowards(objTransform.position, targetPosition, speed * Time.deltaTime);
            yield return null;
        }

       
        Destroy(objTransform.gameObject);
    }


    private IEnumerator Teleport()
    {
        HpBar();
        lockAnimation = false;
        if (animator != null)
        {
            animator.speed = 1f;
        }
        
        shotCounter = 0;
        yield return new WaitForSeconds(0.5f);
        spriteRenderer.enabled = false;
        yield return new WaitForSeconds(0.5f);
        transform.position = destinationTransform.position;
        yield return new WaitForSeconds(0.5f);
        spriteRenderer.flipX = false;
        spriteRenderer.enabled = true;
        yield return new WaitForSeconds(1f);
        ChooseRandomAttack();
    }

    private IEnumerator DpsPhase()
    {


        yield return new WaitForSeconds(0.5f);
       
        spriteRenderer.enabled = false;
       
        yield return new WaitForSeconds(0.5f);
       
        transform.position = dpsTransform.position;
      
        yield return new WaitForSeconds(0.5f);
       
        spriteRenderer.enabled = true;
      
        yield return new WaitForSeconds(1f);
       
        dpsAvailable = true;
      
        yield return new WaitForSeconds(1.5f);
       
        dpsAvailable = false;
        

        atkmuber = 0;
        
        StartCoroutine(Teleport());
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !playerEntered)
        {
            
            playerEntered = true;
            magusDoor.SetActive(true);
            TriggerBossAttack();
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player") && playerEntered)
        {

            playerEntered = false;
            
        }
    }
}
