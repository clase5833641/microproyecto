using UnityEngine;

public class Knockback : MonoBehaviour
{
    public float knockbackStrength = 5f;
    public float knockbackDuration = 0.2f;

    private Rigidbody2D rb;
    private float knockbackTimer;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void KnockbackEnemy(Vector2 direction)
    {
        knockbackTimer = knockbackDuration;
        rb.velocity = direction * knockbackStrength;
    }

    void Update()
    {
        if (knockbackTimer > 0)
        {
            knockbackTimer -= Time.deltaTime;
            if (knockbackTimer <= 0)
            {
                rb.velocity = Vector2.zero;
            }
        }
    }
}
