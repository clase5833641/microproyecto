using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;

public class PlayerHealth : MonoBehaviour
{
    private bool isDead = false;
    public GameManagerScripts gameManager;
    [SerializeField] private int health;
    [SerializeField] private int maxHP;
    public AudioSource death;
    void Start()
    {

    }


    void Update()
    {
        
    }

    public void Damage(int amount)
    {
        if (amount < 0)
        {
            throw new System.ArgumentOutOfRangeException("cant have negative health");
        }

        this.health -= amount;

        if ((this.health <= 0) && !isDead)
        {
            isDead = true;
            gameManager.GameOver();
            Die();
        }
    }

    public void Heal(int amount)
    {
        if (amount < 0)
        {
            throw new System.ArgumentOutOfRangeException("cant have negative healing");
        }

        if (health + amount > maxHP)
        {
            this.health = maxHP;
        }
        else
        {
            this.health += amount;
        }
    }
    private void Die()
    {
        gameManager?.GameOver();
        if (death != null) 
        {
            death.Play();
        }
        gameObject.SetActive(false);
    }
}

