using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    public AudioSource audioSource; 
    public AudioClip[] musicTracks;
    private int currentIndex = 0;
    private bool[] trackPlayed; 

    void Start()
    {
      
        trackPlayed = new bool[musicTracks.Length];
        for (int i = 0; i < trackPlayed.Length; i++)
        {
            trackPlayed[i] = false;
        }

        
        ShuffleArray(musicTracks);

       
        PlayNextTrack();
    }

    void Update()
    {
       
        if (!audioSource.isPlaying)
        {
          
            PlayNextTrack();
        }
    }

    void PlayNextTrack()
    {
        
        while (trackPlayed[currentIndex])
        {
            currentIndex++;
            
            if (currentIndex >= musicTracks.Length)
            {
                ShuffleArray(musicTracks);
                currentIndex = 0;
            }
        }

      
        audioSource.clip = musicTracks[currentIndex];
        audioSource.Play();

        
        trackPlayed[currentIndex] = true;
    }

 
    void ShuffleArray<T>(T[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            int randomIndex = Random.Range(i, array.Length);
            T temp = array[i];
            array[i] = array[randomIndex];
            array[randomIndex] = temp;
        }
    }
}
