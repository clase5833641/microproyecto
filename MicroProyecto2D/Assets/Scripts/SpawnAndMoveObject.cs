
using UnityEngine;

public class SpawnAndMoveObject : MonoBehaviour
{
    public GameObject objectToActivate;
    public GameObject spawnXPositionObject;
    public GameObject objectToSpawn;
    public float spawnYPosition;
    public float moveUpAmount = 3.0f;
    private Transform playerTransform;
    private bool isAttack1InProgress = false;
    public Animator enemyAnimator;
    public AudioSource laugh1;
    public AudioSource laugh2;

    void Start()
    {
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        if (playerObject != null)
        {
            playerTransform = playerObject.transform;
        }
    }

    public void StartSpawningProcess()
    {
        if (!isAttack1InProgress && playerTransform != null)
        {
            objectToActivate.SetActive(true);
            Invoke("SpawnObject", 0.5f);
            isAttack1InProgress = true;
        }
    }

    void SpawnObject()
    {
        if (playerTransform != null)
        {
            float spawnDelay = Random.Range(2.5f, 7.0f);
            Invoke("Spawn", spawnDelay);
        }
        else
        {
            ResetAttackState();
        }
    }

    void Spawn()
    {
        if (playerTransform != null && enemyAnimator != null)
        {
            enemyAnimator.SetBool("IsAttacking", true);

          
            float spawnXPosition = spawnXPositionObject.transform.position.x;
            Vector3 spawnPosition = new Vector3(spawnXPosition, spawnYPosition, transform.position.z);

        
            int randomNumber = Random.Range(1, 3);
            switch (randomNumber)
            {
                case 1:
                    if (laugh1 != null)
                    {
                        laugh1.Play();
                    }
                    break;
                case 2:
                    if (laugh2 != null)
                    {
                        laugh2.Play();
                    }
                    break;
            }
            

            
            GameObject spawnedObject = Instantiate(objectToSpawn, spawnPosition, Quaternion.identity);

            spawnedObject.GetComponent<Collider2D>().enabled = false; 

         
            float moveUpDistance = 0.3f; 
            Vector3 moveUpPosition = spawnPosition + Vector3.up * moveUpDistance;
            float moveUpDuration = 0.2f;
            LeanTween.move(spawnedObject, moveUpPosition, moveUpDuration).setOnComplete(() =>
            {
             
                float waitTime = 0.4f;
                spawnedObject.GetComponent<Collider2D>().enabled = true;
                LeanTween.delayedCall(waitTime, () =>
                {
                   
                    LeanTween.moveY(spawnedObject, spawnYPosition + moveUpAmount, 1.5f)
                        .setEase(LeanTweenType.easeOutQuad)
                        .setOnComplete(() =>
                        {
                            
                            OriginalSpawnMethod(spawnedObject);
                        });
                });
            });
        }
        else
        {
            ResetAttackState();
        }
    }

    void OriginalSpawnMethod(GameObject spawnedObject)
    {
        LeanTween.moveY(spawnedObject, spawnYPosition, 0.5f).setEase(LeanTweenType.easeOutQuad).setOnComplete(() =>
        {
            Destroy(spawnedObject);
            if (objectToActivate != null)
            {
                objectToActivate.SetActive(false);
            }
            isAttack1InProgress = false;
        });
    }

    void ResetAttackState()
    {
        Invoke("ResetTrigger", 1.5f);
    }

    void ResetTrigger()
    {
        if (enemyAnimator != null)
        {
            enemyAnimator.ResetTrigger("IsAttacking");
        }

        if (objectToActivate != null)
        {
            objectToActivate.SetActive(false);
        }
        isAttack1InProgress = false;
    }
}