using UnityEngine;

public class TriggerAreaMagus : MonoBehaviour
{
    private BossController bossController;
    public GameObject magusDoor;

    public void SetBossController(BossController controller)
    {
        bossController = controller;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Trigger entered");
        if (other.CompareTag("Player") && bossController != null)
        {
            magusDoor.SetActive(true);
            bossController.TriggerBossAttack();
        }
    }
}
